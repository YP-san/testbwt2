<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeatherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weather', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->string('todaydate',255);
			$table->string('tomorowdate',255);
			$table->string('nowtempC',9);
			$table->string('nowtempF',9);
			$table->string('todaytempfromC',9);
			$table->string('todaytemptoC',9);
			$table->string('tommorowtempfromC',9);
			$table->string('tommorowtemptoC',9);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weather');
    }
}
