<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',['as'=>'mainpage','uses'=>'MainPageController@show']);
Route::get('/weather',['middleware'=>['auth'],'uses'=>'WeatherController@show','as'=>'weather']);

Route::get('/feedbackform',['uses'=>'FeedbackformController@show','as'=>'feedbackform']);
Route::post('/feedbackform',['uses'=>'FeedbackformController@store','as'=>'feedbackformStore']);

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
