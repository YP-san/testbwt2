<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Weather extends Model
{
    protected $table = 'weather';
    public $primaryKey = 'id';
    public $fillable = ['nowtempC','nowtempF'];
    public $timestamps = true;
}
