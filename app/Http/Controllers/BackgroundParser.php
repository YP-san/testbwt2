<?php

namespace App\Http\Controllers;

use App\Weather;
use phpQuery;
use Illuminate\Support\Facades\DB;

class BackgroundParser extends Controller
{
    private $flag;
    public function setFlag($flag){
        return $this->flag = $flag;
    }
    public function getFlag(){
        return $this->flag;
    }

    public function getContent (){
        $html = file_get_contents('http://www.gismeteo.ua/city/daily/5093/');
        $pq= phpQuery::newDocument($html);
        $query =  $pq->find('.date');
        foreach ($query as $date){
            $dates [] = trim(pq($date)->text());
        }
        $query = $pq->find('.tab-weather__value_l');
        foreach ($query as $item){
            $nowtemp[] = trim(pq($item)->text());
        }
        $query = $pq->find('body > 
            section > 
            div.content_wrap > 
            div > 
            div.main > 
            div > 
            div.__frame_sm > 
            div.forecast_frame.hw_wrap > 
            div.tabs._center > div > div > 
            div.tab-content > 
            div.tabtempline.tabtemp_0line.clearfix > 
            div > 
            div > 
            div > 
            div > 
            span.unit.unit_temperature_c');
        foreach ($query as $item){
            $todaytemp[] =trim(pq($item)->text());
        }
        $query = $pq->find('body > 
            section > 
            div.content_wrap > 
            div > 
            div.main > 
            div > 
            div.__frame_sm > 
            div.forecast_frame.hw_wrap > 
            div.tabs._center > 
            a.nolink.tab.tablink.tooltip > 
            div > div.tab-content > 
            div.tabtempline.tabtemp_1line.clearfix > 
            div > 
            div > 
            div > 
            div > 
            span.unit.unit_temperature_c');
        foreach ($query as $item) {
            $tomorrowtemp[] = trim(pq($item)->text());
        }
        $parsedarray = [
            'dates'=>$dates,
            'nowtemp'=>$nowtemp,
            'todaytemp'=>$todaytemp,
            'tomorrowtemp'=>$tomorrowtemp
        ];
        phpQuery::unloadDocuments();
        return $parsedarray;
    }

    private function dbChecker(){
        $data = $this->getContent();
        $datebuf = [
            $data['dates'][3],
            $data['dates'][2]
        ];
        $today = implode(' - ',$datebuf);
        $lastrec = DB::table('weather')->
        select('todaydate','id')
            ->orderBy('id','desc')
            ->limit('1')
            ->get()->toArray();
        if(!empty($lastrec)){
            $lastrec = json_decode(json_encode($lastrec[0]),true);
            if($today == $lastrec['todaydate']){
                return $this->setFlag($lastrec['id']);}
            else{
                return $this->setFlag(false);}
        }
        else{
            return false;}
      }

    public function saveContent(){
        $data = $this->getContent();
        $this->dbChecker();
        $datebuf = [
            $data['dates'][3],
            $data['dates'][2]
        ];
        $today = implode(' - ',$datebuf);
        $datebuf = [
            $data['dates'][5],
            $data['dates'][4]
        ];
        $tomorow = implode(' - ',$datebuf);
        $nowtempC = $data['nowtemp'][0];
        $nowtempF = $data['nowtemp'][1];
        $todaytempfrom = $data['todaytemp'][0];
        $todaytempto = $data['todaytemp'][1];
        $tomorowtempfrom = $data['tomorrowtemp'][0];
        $tomorowtempto = $data['tomorrowtemp'][1];
        if($this->getFlag() == false){
            $weather = new Weather;
            $weather->todaydate = $today;
            $weather->tomorowdate = $tomorow;
            $weather->nowtempC=$nowtempC;
            $weather->nowtempF=$nowtempF;
            $weather->todaytempfromC = $todaytempfrom;
            $weather->todaytemptoC = $todaytempto;
            $weather->tommorowtempfromC = $tomorowtempfrom;
            $weather->tommorowtemptoC = $tomorowtempto;
            $weather->save();
        }
        else{
            $weather = Weather::all()->find($this->getFlag());
            $weather->nowtempC = $nowtempC;
            $weather->nowtempF = $nowtempF;
            $weather->save();
        }
    }




}
