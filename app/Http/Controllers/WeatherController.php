<?php

namespace App\Http\Controllers;

use App\FeedBack;
use App\Weather;
use App\Http\Controllers\BackgroundParser;
use Illuminate\Support\Facades\DB;

class WeatherController extends Controller
{
    public function show()
    {
            $feedbackarray = FeedBack::all()->toArray();
            $lastrec = DB::table('weather')->
            select('*')
            ->orderBy('id','desc')
            ->limit('1')
            ->get()->toArray();
            $lastrec = json_decode(json_encode($lastrec[0]),true);

            $nowtime =  date("H:i:s");
        $output = [
                'dates'=>['Сейчас',$nowtime, $lastrec['todaydate'], $lastrec['tomorowdate']],
                'nowtemp'=>[$lastrec['nowtempC'],$lastrec['nowtempF']],
                'todaytemp'=>[$lastrec['todaytempfromC'],$lastrec['todaytemptoC']],
                'tomorrowtemp'=>[$lastrec['tommorowtempfromC'],$lastrec['tommorowtemptoC']]
            ];

          return view('weather',[
              'feedbackarray'=>$feedbackarray,
              'dates'=>$output['dates'],
              'nowtemp'=>$output['nowtemp'],
              'todaytemp'=>$output['todaytemp'],
              'tomorrowtemp'=>$output['tomorrowtemp']
          ]);

    }
}

