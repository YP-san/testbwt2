@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    You are logged in!
                        <p><a class="btn btn-secondary" href="{{route('weather')}}" role="button" style="margin-top: 2vh;width: 155px">Show me a weather !! </a></p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
