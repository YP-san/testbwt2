@extends('weather')
<head>
    <title>Feedback form</title>
</head>
@section('header')
    @parent
    @endsection
@section('content')
    <main role="main" style="margin-top: 10vh">
<div class="container">
    <div class="row">
        {{--<div class="col-md-4"> </div>--}}
        <div class="col-md-4">
            <h3>Leave your feed back about my app =)</h3>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="{{route('feedbackformStore')}}" method="post">
                @csrf
                <div class="form-group">
                    <label for="name">Your name:</label>
                    <input type="text" name="name" class="form-control" id="name" placeholder="Name" value="{{old('name')}}">
                </div>
                <div class="form-group">
                    <label for="email1">E-mail:</label>
                    <input type="email" name="email" class="form-control" id="email" placeholder="Email" value="{{old('email')}}">
                </div>
                <div class="form-group">
                    <label for="message">Message:</label>
                    <textarea class="form-control" name="message" rows="3">{{old('message')}}</textarea>
                </div>
                <button type="submit" class="btn btn-info">Отправить сообщение</button>
            </form>
        </div>
        <div class="col-md-4"> </div> </div>
</div>
    </main>
    @endsection
@section('footer')
    @parent
    @endsection

